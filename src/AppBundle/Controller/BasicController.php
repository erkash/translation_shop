<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Phrase;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class BasicController extends Controller
{
    /**
     * @Route("/locale/{_locale}", requirements = {"_locale" : "en|ru"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function setLocaleAction(Request $request)
    {
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('ruDesc', TextType::class, ['label' => 'фраза на русском'])
            ->add('save', SubmitType::class, ['label' => 'сохранить'])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $phrase = new Phrase();
            $phrase->translate('ru')->setDescription($data['ruDesc']);

            $em = $this->getDoctrine()->getManager();
            $phrase->setUser($this->getUser());
            $em->persist($phrase);
            $phrase->mergeNewTranslations();
            $em->flush();
        }

        $phrases = $this->getDoctrine()->getRepository('AppBundle:Phrase')->findAll();

        return $this->render('AppBundle:Basic:index.html.twig', [
            'form' => $form->createView(),
            'phrases' => $phrases
        ]);
    }

    /**
     * @Route("/phrases/{id}", requirements={"id": "\d+"})
     * @param Request $request
     * @param $id integer
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailAction(Request $request, int $id) {
        $phrase = $this->getDoctrine()
            ->getRepository('AppBundle:Phrase')
            ->find($id);
        $locales = [
            'en' => null,
            'fr' => null,
            'de' => null,
            'kg' => null,
            'ru' => null
        ];

        $form = $this->createFormBuilder();
        foreach ($locales as $key => $locale) {
            foreach ($phrase->getTranslations() as $translation) {
                if($key == $translation->getLocale()){
                    $locales[$key] = $translation->getDescription();
                }
            }

            if($locales[$key] == null) {
                $form->add($key, TextType::class, ['required' => false]);
            }
        }

        $form = $form->add('save', SubmitType::class, ['label' => 'Сохранить'])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $locales = array_keys($data);
            $locale_index = 0;
            foreach ($data as $datum){
                if ($datum != null){
                    $phrase->translate($locales[$locale_index])->setDescription($datum);
                }
                $locale_index++;
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($phrase);

            $phrase->mergeNewTranslations();
            $em->flush();
            return $this->redirectToRoute('app_basic_detail', ['id' => $id]);
        }

        return $this->render('AppBundle:Basic:detail.html.twig', [
            'forms' => $form->createView(),
            'locales' => $locales
        ]);
    }
}
